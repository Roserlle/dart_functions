num add(num x, num y) {
    return x + y;
}

Map<String, num> switchNumber(num num1, num2) {
    // No new variables are allowed.
    num1 = num1 + num2;
    num2 = num1 - num2;
    num1 = num1 - num2;
    return {
        'num1': num1,
        'num2': num2
    };
}

int? countLetter(String letter, String sentence) {
    var res = letter.allMatches(sentence).length;

    return (res == 0 ? null : res);
}

bool isPalindrome(String text) {
    String str = text.split(' ').join().toLowerCase();
    String reverse = '';

    for(int i = 0; i < str.length; i++){
        reverse = reverse + str[(str.length - 1) - i];
    }

    if (str == reverse) {
        return true;
    } else {
        return false;
    }
}

bool isIsogram(String text) {
    var str = text.toLowerCase();
    var len = str.length;
    var arr = str.split('');

    arr.sort();

    for(int i = 0; i < len - 1; i++){
        if (arr[i] == arr[i + 1]){
            return false;
        }
    }
    return true;
}

num? purchase(int age, num price) {
    num discountedPrice = price * 0.8;
    num roundedPrice = num.parse(discountedPrice.toStringAsFixed(2));
    
    if (age < 13){
        return null;
    } else if (age >= 13 && age <= 21){
        return roundedPrice;
    } else if (age >= 22 && age <= 64){
        return num.parse(price.toStringAsFixed(2));
    } else if (age > 64 ){
        return roundedPrice;
    } else{
        return null;
    }

}


List<String> findHotCategories(List items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].

    List<String> res = [];

    for (var item in items) {
        if(item['stocks'] == 0){
            res.add(item['category']);
        }
    }

    return res.toSet().toList();
}

List<String> findFlyingVoters(List candidateA, List candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    
    List<String> res = [];

    for(int j = 0 ; j < candidateA.length; j++){
        for(int i = 0; i < candidateB.length; i++){
            if(candidateA[j] == candidateB[i]){
                res.add(candidateA[j]);
            }
        }
    }

    return res.toSet().toList();
}

List<Map<String, dynamic>> calculateAdEfficiency(List items) {
    // Sort the ad efficiency according to the most to least efficient.

    // The passed campaigns array from the test are the following:
    // { brand: 'Brand X', expenditure: 12345.89, customersGained: 4879 }
    // { brand: 'Brand Y', expenditure: 22456.17, customersGained: 6752 }
    // { brand: 'Brand Z', expenditure: 18745.36, customersGained: 5823 }

    // The efficiency is computed as (customersGained / expenditure) x 100.

    // The expected output after processing the campaigns array are:
    // { brand: 'Brand X', adEfficiency: 39.51922461645131 }
    // { brand: 'Brand Z', adEfficiency: 31.063687227132476 }
    // { brand: 'Brand Y', adEfficiency: 30.06746030155632 }

    List<Map<String, dynamic>> brandEfficiency = [];
    
    items.forEach((item) {
        brandEfficiency.add({
           'brand' : item['brand'],
           'adEfficiency' : item['customersGained']/item['expenditure'] * 100
        }); 
    });

    brandEfficiency.sort((a, b) => b["adEfficiency"].compareTo(a["adEfficiency"]));

    return brandEfficiency;
}